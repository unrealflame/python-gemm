import unittest
import Convolutions
import numpy as np

class ConvolutionTests(unittest.TestCase):
    def testGenMatrix(self):
        Result = Convolutions.GenMatrix(5, 5, 10)
        Expected = np.full((5, 5), 10)
        self.assertTrue((Result == Expected).all())

if __name__ == '__main__':
    unittest.main()
