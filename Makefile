CC := python3.6

build:
	$(CC) Convolutions.py

test:
	$(CC) UnitTests.py
