import numpy as np
import random
from pprint import pprint

def GenMatrix(Height, Width, InitialVal = 0):
    Matrix = []
    for i in range(Height):
        Matrix.append([])
        for j in range(Width):
            Matrix[i].append(InitialVal)

    return np.array(Matrix, dtype=float)

def RandomiseMatrix(Height, Width, Min, Max):
    Matrix = GenMatrix(Height, Width)
    for x in range(Height):
        for y in range(Width):
            Matrix[x, y] = random.randint(Min, Max)
    return Matrix

def ExtractValues(Matrix, Dimensions, x, y):
    Values = []
    for i in range(Dimensions[0]):
        for j in range(Dimensions[1]):
            Values.append(Matrix[x + i, y + j])
    return Values

def im2col(Matrix, Dimensions, Stride = 1):
    # Calculate the height and width of the Matrix
    MatrixHeight = len(Matrix)
    MatrixWidth = len(Matrix[0])

    # Ensure the dimensions are 2
    assert len(Dimensions) == 2, "The dimensions for im2col should be a 2D tuple."
    KernelHeight = Dimensions[0]
    KernelWidth = Dimensions[1]

    # Calculate the number of kernels we can fit each way
    VerticalKernels = int((MatrixHeight - KernelHeight) / Stride) + 1
    HorizontalKernels = int((MatrixWidth - KernelWidth) / Stride) + 1

    # Calculate the size of the resulting matrix
    ResultHeight = HorizontalKernels * VerticalKernels
    ResultWidth = KernelHeight * KernelWidth

    Result = []
    for x in range(0, MatrixHeight - Stride, Stride):
        for y in range(0, MatrixWidth - Stride, Stride):
            Result.append(ExtractValues(Matrix, Dimensions, x, y))

    return Result


def DotProduct(Matrix, Kernel, x, y):
    Total = 0.0
    for i in range(len(Kernel)):
        for j in range(len(Kernel[0])):
            Total += Matrix[x + i, y + j] * Kernel[i, j]
    return Total

def Convolve(Matrix, Kernel, Stride = 1):
    # Calculate the height and width of the Matrix
    MatrixHeight = len(Matrix)
    MatrixWidth = len(Matrix[0])

    # Calculate the height and width of the Kernel
    KernelHeight = len(Kernel)
    KernelWidth = len(Kernel[0])

    # Calculate the height and width of the convolved matrix
    ConvolvedHeight = int((MatrixHeight - KernelHeight) / Stride + 1)
    ConvolvedWidth = int((MatrixWidth - KernelWidth) / Stride + 1)

    # Ensure the whole matrix is filled
    Offset = 1 if Stride != 1 else 0

    # Create our new convolved matrix, initialising with 0s
    Convolved = GenMatrix(ConvolvedHeight, ConvolvedWidth)

    # Iterate across the matrix and perform dot products with the kernel
    for x in range(0, ConvolvedHeight + Offset, Stride):
        for y in range(0, ConvolvedWidth + Offset, Stride):
            Convolved[int(x / Stride), int(y / Stride)] = DotProduct(Matrix, Kernel, x, y)

    return Convolved
